'''
@file main7.py

This file will act on the Nucleo to allow Motor and Encoder operation.

This file will always be on to run the FSM in EastSideFSM. The FSM itself will regulate the system response.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 26, 2020
'''
import pyb

from Lab7Nucleo import Operator
'''
@brief   Define all the pins used by our motor and encoder
'''
pin_nSLEEP = pyb.Pin.cpu.A15; 
pin_IN1 = pyb.Pin.cpu.B0; 
pin_IN2 = pyb.Pin.cpu.B1;
tim = 3;

pin1 = pyb.Pin.cpu.C6
pin2 = pyb.Pin.cpu.C7
enctimer = 8


task1 = Operator(pin_nSLEEP, pin_IN1, pin_IN2, tim, pin1, pin2, enctimer)
while True:
    '''
    @brief   Continuously run task1 which is our Nucleo Side code
    '''
    task1.run()