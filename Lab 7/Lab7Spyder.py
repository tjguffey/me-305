'''
@file Lab7Spyder.py
This file provides the coding needed to operate an motor and encoder.

This file runs the same FSM from Lab 6, except that it now should run through 
the matrix of values provided in the reference file and it will stop once these
values have been exhausted. The program will then plot the resulting speeds as
a function of time along with the actual values from the reference matrix.
This is all in theory of course. Unfortunately I haven't actually been able to
complete a lab all the through almost at all this quarter.

@image html Lab7Profile.png

@author Tyler J. Guffey

@copyright TG-Coding

@date December 3, 2020
'''

import time
import serial
import plt
import pandas as pd

## Import csv values
data = pd.read_csv (r'/Users/tylerguffey/Desktop/ME305/Lab 7/reference.csv')
## Take the important values (Angular Velocity [RPM])
omeg = pd.DataFrame(data, columns= ['30'])
## Convert RPM to Angle/Second (1rev = 360 deg) (1 min = 60 sec)
ref = omeg/360*60

class ClosedLoop:
    '''
    @brief      A finite state machine to control a motor.
    @details    This class implements a finite state machine to control the
                operation of a motor by use of an encoder input and gain.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_ADAPT   = 1    
    
    ## Constant defining State 2
    S2_SEND    = 2    
    
    ## Constant defining State 3
    S3_WAIT    = 3    
    
    ## Constant defining State 4
    S4_PREP = 4
    
    ## Constant defining State 5
    S5_PLOT    = 5
    
    def __init__(self, load, ref, kp, port):
        '''
        @brief            Creates a ClosedLoop object.
        @param load       An object from class ClosedLoop representing the Initial Load (L)
        @param ref        An object from class ClosedLoop representing the reference omegas
        @param kp         An object from class ClosedLoop representing the gain factor (kp')
        @param length     An object from class ClosedLoop representing the length of time to run 
        @param port       An object from class Serial representing the serial port used
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The inital load we want to set the motor at; this will be updated
        self.load = self.load
        
        ## This is the reference omega
        self.ref = self.ref
        
        ## This is the kp or gain
        self.kp = self.kp
        
        ## The overall amount of runs to be done (One less than length of values since we keep referring to the next val to correct the motor)
        self.length = len(self.ref)-1
        
        ## The serial port set up for this lab
        ser = self.port  #Should be '/dev/tty.usbmodem14103'
        self.serial = serial.Serial(port = ser, baudrate = 115273, timeout = 1)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = 0.001
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## The time values of each recorded value
        self.xvals = []
        
        ## The omega at each each recorded moment
        self.yvals = []
        
        ## The reference omega
        self.yvals2 = []
        
    def run(self,value):
        '''
        @brief     Runs one iteration of the task
        @details   ClosedLoop.run() will first check the duty meant to be applied,
                   and it will assume that a value between 0 and 1 is a mistake
                   and corrects it by a factor of 100. This also will keep a
                   duty cycle of more than 100 to be limited to 100. Next, this 
                   value is sent to the Nucleo, and the duty cycle is applied 
                   to the motor. After this, the Nucleo waits for a response,
                   and once one is recieved, the operation analyzes the speed
                   of the motor and adjusts the duty cycle as it sees fit.
                   After this new duty is determined, the cycle is repeated.
                   After a certain interval of time, the program stops sending
                   out corrected values and plots the changes of the system.
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            ## This code is launched when the system is started.
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_ADAPT)
            
            ## Adjust the duty cycle input to be usable.
            elif(self.state == self.S1_ADAPT):
                if self.load<1 and self.load > 0:
                    self.load = self.load*100
                elif self.load>-1 and self.load < 0:
                    self.load = self.load*100
                elif self.load>100:
                    self.load = 100
                elif self.load<-100:
                    self.load = -100
                else:
                    pass
                ## After adjusting the duty cycle, move to State 2.
                self.transitionTo(self.S2_SEND)
            
            ## State 2: Send the duty values off to the nucleo
            elif(self.state == self.S2_SEND):
                # Run State 2 Code
                self.sendchar(self.load)
                ## After sending the new duty value, transition to state 3.
                self.transitionTO(self.S3_WAIT)
            
            ## State 3: Wait for a value unless a certain time has passed
            elif(self.state == self.S3_WAIT): # Should be sent back a new load
                ## If the program has run through enough times, move on.
                if self.runs>=self.length:
                    self.transitionTo(self.S4_PREP)
                ## If enough time hasn't passed, keep searching for a value
                else:
                    self.savechar()
                    ## If a new value is detected, adjust to gain a new duty val using next omega val
                    self.load = self.kp*(self.ref[self.runs]-self.yvals[-1])
                    ## After adjusting, move backk to state 1.
                    self.transitionTo(self.S1_ADAPT)

            ## State 4: Prep the values to be plotted
            elif(self.state == self.S4_PREP):
                self.xvals = self.xvals
                self.yvals = self.yvals
                ## After prepping the values, move to state 5
                self.transitionTo(self.S5_PLOT)
            
            ## State 5: Plot our values from the run
            elif(self.state == self.S5_PLOT):
                ## First plot experienced omega over time
                self.plot4u(self.xvals, self.yvals)
                ## Hold on so it plots both plots on same graph
                plt.hold_on
                ## Plot our reference omega over time
                self.plot4u(self.xvals, self.yvals2)
                ## Move on to state 6
                self.transitionTo(self.S6_END)
            
            ## State 6: The process is over and our machine sits in Limbo
            elif (self.state == self.S6_END):
                None
                # Some clever exit strategy
            
            else:
                # Invalid state code (error handling)
                pass
                
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
     
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


    def sendchar(self,inv):
        '''
        @brief     Sends an input to the serial
        '''
        #inv = input('If you wish to continue, please press G; Enter any other key to cancel: ')
        self.serial.write(str(inv).encode('ascii')) 


    def savechar(self):
        '''
        @brief     Saves two character detected in the serial into two matrices
        '''
        input_list = []
        ## If there is an input in the serial port, continue saving it
        if self.serial.readline() !=0:
            #ser.readline().decode('ascii') from Piazza
            input_string = self.serial.readline()
            input_list = input_string.strip('\r\n').split('\r\n')#split(',') or strip('\r\n')?
            self.xvals.append(float(input_list[0]))
            self.yvals.append(float(input_list[1]))
            self.yvals2.append(self.ref[self.runs])
            

    def plot4u(self, one, two):
        '''
        @brief     Plots a graph with the below labels
        '''
        plt.plot(one, two, 'r-')
        plt.xlabel('Time [Seconds]')
        plt.ylabel('Position [Encoder Ticks]')
        plt.show()   


'''
Below we define the inital duty load, reference omega, gain factor, length of
time for the cycle, and the port that the Nucleo is connected to.
'''

load = 35
#ref = 30
kp = 0.1
length = 5
port = '/dev/tty.usbmodem14103'

'''
Below we define the ClosedLoop operation as 'task' and we run it.
'''
task = ClosedLoop(load, ref, kp, length, port)
while True:
    task.run()

# I haven't really been able to get most of my labs to actually work on the 
# Nucleo. As much as I wish this would work, it doesn't for me. I hope you can
# figure this out and that its a small issue that I'm just not seeing.










