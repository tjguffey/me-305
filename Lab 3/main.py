## @file main.py
# This file mimics the coding needed to operate an Encoder (Main Page).
#
# This lab assignment uses an Encoder and its Timer Channels to keep track of 
# its position. The below code imports commands from the Encoder.py file and
# responds to inputs provided by the user.
#
# The code is dependent on a constantly running motor, and the code is not 
# autonomous. It will keep track of what happens as fast as you can tell it to
# check... For now.
#
# In additiion to the commands and inputs desired in the Lab handout, a few
# more are present in an effort to improve practice with user-system interface.
# If possible, 'input' (Coding Pun, Apologies) would be appreciated on the
# functionality of the interface provided. Thanks, and Enjoy.
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date October 19, 2020

import pyb
import Encoder
from pyb import UART

## This code will run when the system is rebooted.
if __name__ == '__main__':
    ## Below uses the Encoder class to create an Encoder Object
    #
    # The "encode" line just uses the __innit__ lines of the Encoder class to 
    # set up an encoder with the pins and timer channels specified. The 
    # 'timers' are also initialized and awaiting input.
    encode = Encoder.Encoder(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3, 3)
    print('Created Running Encoder Profile')
    print('You may input the letters z, p, d, r, and t to zero the coder position, display the encoder position, display the most recent encoder delta, reset the system, and display the overall encoder delta respectively')
    ## myart uses UART(2) to enable keyboard inputs
    myuart = UART(2)
    while True:
        ## Below encompasses Task 2- User Interface
        if myuart.any() !=0:
            ## read is the current input 
            read = myuart.readchar()
            ## Press z to zero the counter position
            if read==122:
                encode.soft_reset()
            ## Press p to display the encoder position
            elif read==112:
                encode.get_position()
            ## Press d to display the most recent delta
            elif read==100:
                encode.get_delta()
                print(Encoder.Encoder.self.delta)
            ## Press t to display overall position change of the encoder
            elif read==116:
                encode.total_change()
            elif myuart()==114:
                encode.full_reset()
            else:
                print('You out yo DAMN MIND BOY: Input Invalid, Please Pick One of the Provided Options (z, p, d, or t)' )
            
