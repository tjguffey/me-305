## @file Encoder.py
# This file mimics the coding needed to operate an Encoder.
#
# This lab assignment uses a class system to write code that will keep track
# of a motor's position through its encoder and its associated pins and timer
# channels. The below file provides code that can set up the appropriate
# timers, update the system, provide that updated position, set a new position,
# provide and check the most recent change, provide the position change 
# encountered while the code has been run, reset the timer to zero while
# maintaining its previous position changes, and fully reset the timer channel
# as well as the position recorded.
#
# In terms of input, this code needs the two pins associated with the timers
# and the channels associated with the board's input pin locations.
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date October 19, 2020

from random import choice
import pyb
from pyb import UART

class EncoderDriver:
    '''
    @brief      An Encoder class
    @details    This class represents an Encoder that the will be adjusted by
                the motor to represent the motor position. As of now, the class
                Is more theoretical as the motor has not been turned on.
    '''
    
    ## Initialize the code
    #
    # Define the pins associated with the encoder's timers and the channels
    # that should be used for the class.
    def __init__(self, pin1, pin2, timer_number):  #A6,A7,3 for little motor; C6, C7, 8
        '''
        @brief      Creates an Encoder Object
        @param pin1  A pin object that the encoder is connected to
        @param pin2  Another pin that is connected to the encoder
        @param timer_number The timer number for the first timer channel
        @param read_number Another timer number for the second timer channel
        '''
        print('Setting Up')
        
        ## The pin object used to read the Button state (pin1 should be A6)
        self.pin1 = pin1 #pyb.Pin(pin1, pyb.Pin.IN)
        
        ## The pin object used to read the Button state (pin2 should be A7)
        self.pin2 = pin2 #pyb.Pin(pin2, pyb.Pin.IN)
        
        ## The timer used to read the Encoder state and associated counter
        self.tim1 = pyb.Timer(timer_number)    #3
        self.tim1.init(prescaler=0, period=65535)
	
	## The channel used for object encoder1
        self.encoder1 = self.tim1.channel(1, pin=self.pin1, mode=pyb.Timer.ENC_AB)
        print('Timer 1 created and attached to pin '+ str(self.pin1))
	
	## The channel used for object encoder2
        self.encoder2 = self.tim1.channel(2, pin=self.pin2, mode=pyb.Timer.ENC_AB) #Encoder 1, Diff Channel
        print('Timer 2 created and attached to pin '+ str(self.pin2))

        ## The total change in position
        self.position = 0
        
        ## The "Current" position
        self.current = 0
        
        ## The initial self.delta to start code on
        self.delta = 0
        
        print('Encoder Initialized')    
        
    def update(self):
        '''
        @brief     Updates Current Position of the Encoder  
        @details   The most recent position of the Encoder is recorded.
        '''
	## self.past is the last value recorded
        self.past = self.current
        self.current = self.tim1.counter()
        self.position = self.position + (0 - self.current)
    
 
        ##
        # Check for good/bad delta
    def get_position(self):
        '''
        @brief     Returns Most Recently Updated Position of the Encoder  
        @details   The encoder is read and the most recent position is
                   recorded, and then displayed.
        '''
        #self.current = self.tim1.counter()
        self.update()
        self.position = self.position + (0 - self.current)
        print('Current Encoder Position: ' + str(self.current) )
        
        
        
        ## Wade's Code
        # return self.position
        
        
    def set_position(self,inp):
        '''
        @brief       Sets Encoder to Input Position  
        @details     The current position is updated, the Encoder is set at 
                     'inp', and a message is displayed. 
        @param inp   The input 'inp' is the position desired to set the 
                     Encoder at.
        '''
        if inp in range(0,65535):
            #self.past = self.current
            #self.current = self.tim1.counter() 
            self.update()               
            self.position = self.position + (0 - self.current)
            self.encoder1.counter(inp)
            print('Position Saved... Encoder Position Reset to:', str(inp))
        else:
            print('ERROR: Invalid Input')
    
        ## Wade's Code
        # self.position = inp
    
    def get_delta(self):
        '''
        @brief      Gets the Most Recent Delta  
        @details    The position is updated, and the most recent delta is 'fact 
                    checked' against the last delta. If it is the same as the
                    last delta, the code is allowed to keep adding to the
                    position normally. If it is different, the difference is
                    analyzed, and the position change is rectified and added as
                    necessary. 
        '''
        self.update()
        if self.current==self.past: #Just Initialized, need to allow update
            pass
        
        else:
            if self.delta==0:   #Still hasn't moved
                pass
            else:
                check = self.delta    #Last Delta
                
            self.delta = self.current-self.past     #Current Delta
            if self.delta==check:   #After Initialize, let run
                self.position = self.position + (0 - self.current)
                pass
            else:
                if abs(self.delta)>(65536/2):  #Bad when dt is >1/2 the Period
                    if check<0: #The encoder was running with negative slope
                        self.position = self.position + (0 - self.current + 65536)
                    elif check==0: #Motor Spins more than T/2 in one count
                        print('Past the Point of No Return: Motor is Spinning Too Fast OR Spun Too Far Since Last Update to Properly Use "get_delta"')
                    else:
                        self.position = self.position + (0 - self.current - 65536)
                        
                else:      #Needs to Run Again
                     pass   
                 
        ## Wade's Code
        # return self.delta
    def total_change(self):
        '''
        @brief     Prints the Total Change Observed  
        @details   The Encoder data is updated and the current total change in
                   position is recorded and printed
        '''
        self.update()
        print('Total Change Observed: ' + str(self.position))
 
    def soft_reset(self):
        '''
        @brief     Sets the Counter Back to Zero  
        @details   This command mimics set_position, except that it lacks an 
                   input. Instead, the Encoder's position is saved, and is
                   reset to the initial 
        '''
        self.current = self.tim1.counter()                  #gain curr pos
        self.position = self.position + (0 - self.current)  #adds pos
        self.encoder1.counter(0)                                #Reset to 0
        print('Change In Position Saved... Encoder Position Reset to Zero')
 
    def full_reset(self):
        '''
        @brief      Fully Resets the Position and Encoder Timer
        @details    This command will respond to an input after displaying a
                    message, and will either reset the system or do nothing.
        @return     
        '''
    
        print('WARNING: This will Reset the System. Press y if this is your intention. If not, press n.')
        ## myart uses UART(2) to enable keyboard inputs
        myuart = UART(2)
        ## Below shows the code for the choices provided
        if myuart.any():  
        ## Press y to reset the position
            if myuart.any()==121:
                self.soft_reset()
                self.position = 0
        ## Press n to cancel input
            elif myuart()==110:
                pass
            else:
                print('ERROR: Please Type y or n')
                myuart.any = []
        

