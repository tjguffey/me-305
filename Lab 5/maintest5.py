# @file maintest5.py
#
#
#


from pyb import UART
import pyb
myuart = UART(3,9600)
A5 = pyb.Pin.cpu.A5
pinA5 = pyb.Pin(A5, pyb.Pin.OUT_PP)

#pinC2 = pyb.Pin(pyb.Pin.cpu.C2, pyb.Pin.OUT_PP)
#pinC2.low()
while True:
    if myuart.any() != 0:
        val = int(myuart.readline()) 
        if val == 0:
            print(val,' turns it OFF')
            pinA5.low() 
        elif val == 1:
            print(val,' turns it ON') 
            pinA5.high()