'''
@file main.py

This file is for the running of our light flashing code. It will take inputs
provided by the bluetooth module and use these inputs to change the blinking
light's frequency.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 11, 2020
'''

import BlinkerUI

task1 = BlinkerUI
while True:
    task1.run()