'''
@file BlinkerUI.py

This file serves as the main function file that will take inputs from the
user's phone and respond to them by blinking lights.

This code will also send messages back to the user based on their choices. The
FSM that this code uses is shown in the attached image.

@image html Lab5FSM.png

@author Tyler J. Guffey

@copyright TG-Coding

@date November 11, 2020
'''
import pyb
import utime
import UARTtool

class BlinkerUI:
    '''
    @brief      A finite state machine to control the main LED with User Input.
    @details    This class implements a finite state machine to control the
                operation of the Nucleo's main LED in pin A5.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_SEARCH           = 1
    
    ## Constant defining State 2
    S2_FLASH            = 2
    
    
    def __init__(self):
        '''
        @brief      Creates a TaskWindshield object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.A5 = pyb.Pin.cpu.A5
        self.pinA5 = pyb.Pin(self.A5, pyb.Pin.OUT_PP)
        
        ## The UART channel needed for this task.
        self.UART = 3
        self.uart = UARTtool(self.UART, 9600)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
    
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(0.2*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
        ## The inital setting of the light
        self.pinA5.low()
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Checking if the timestamp has exceeded our "scheduled" timestamp
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):

            ## Run State 0 Code
            if(self.state == self.S0_INIT):
                print('Protocol Initiated: Searching for Values')
                self.transitionTo(self.S1_SEARCH)
            
            ## Run State 1 Code
            elif(self.state == self.S1_SEARCH):
                ## Updates Characters from the UART
                self.task1 = self.uart.update()
                if self.task1.use():
                    self.transitionTo(self.S2_Flash)
                    self.interval = 1/self.task1.use()
                    print('Flashing at a Frequency of ', str(1/self.interval))
                    
            ## Run State 2 Code
            elif(self.state == self.S2_FLASH):
                ## Updates the uart character to check for new input
                self.task1 = self.uart.update()
                ## Updates the frequency
                #
                # If a frequency over 50 or under 0 is input, the LED is reset; 
                # any other frequency, and the sytem shuts off and waits for a 
                # useable value
                if self.task1.use():
                    ## Checks if the Frequency is above 50 or under 0 Hz
                    if self.task1.use() >= 50 or self.task1.ues()<0:
                        self.transitionTo(self.S1_SEARCH)
                        print('The value entered is too high, please choose another frequency lower than 50 Hz')
                        self.pinA5.low()
                    ## Checks if the frequency is between 0 and 50 Hz
                    elif self.task1.use() >0 and self.task1.use()<50:
                        self.interval = 1/self.task1.use()
                        print('Now Flashing at a Frequency of ', str(1/self.interval))
                
                ## Check if the self.rounds is even or odd
                #
                # If self.rounds is odd, LED is off, Even means on
                if self.runs/2<round(self.runs/2):
                    self.pinA5.low()
                else:
                    self.pinA5.high()
            
            ## This is the Error state
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            ## Counts the runs through the code
            self.runs += 1
            ## Decides the next time we want the code to run at
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        