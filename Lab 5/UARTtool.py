'''
@file UARTtool.py

This file serves as a file that can be used to do various actions using the
UART functions of micropython.

The user can use this file and associated commands to read and write to the 
UART and it will be made primarily with the focus of controlling an LED on our
class Nucleo Board
'''

from pyb import UART

class UARTtool:
    '''
    @brief      A set of options to effectively use the UART machine.
    @details    This class implements a few operations that can make using the
                UART features much simpler.
    '''
    
    def __init__(self, myuart, baud):
        '''
        @brief      Creates a UARTtool object.
        @param  myuart  This should be the UART channel associated with the 
                        UART desired to be read from and written on using this
                        Task.
        @param  baud    This is the baud rate required for the system                
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The UART that this system is using
        self.myuart = UART(myuart, baud)
        
        ## The initial character that is in our "queue"
        self.char = 0
    
        ## The initial character that is ready for use; None at first
        self.use = None
        
        
    def check(self):
        if self.myuart.readchar() !=0 and self.myuart.readchar()>0:
            self.char = 1
            
        else:
            self.char = 0
                
    def read(self):
        if self.char():
            if self.char == 0:
               #print('No Usable Characters in the Current Roster')
               self.use = None
               pass
            else:
                self.use = self.myuart.readchar()
                         
    def write(self, inputs):
        self.update()
        self.myuart.write(inputs)
        
    def update(self):
        '''
        @brief      Runs one iteration of the task
        @details    This will run the task of checking and updating the 
                    character roster
        '''
        self.check()
        if self.char !=0:
            self.read()
