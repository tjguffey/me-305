## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This project was meant to incorporate the famous Fibonacci sequence into a 
#  coding exercise in order to introduce basic programming and commentary on 
#  code through Doxygen. The lab began with a basic testing exercise, and then 
#  the Fibonacci code. These are described in detail below. You can find the 
#  source code for these two files at the following url: 
#  https://bitbucket.org/tjguffey/me-305/src/master/Lab%201/
#
#  @section sec_mot Test File
#  This section was sort of a prelab that was meant to provide a basis of 
#  understanding for the coding language. The code asks for input and provides 
#  an output if the right input is provided. Please see testlab1.my_string which 
#  is part of the \ref testlab1.py package.
#
#  @section sec_enc Lab 0x00: Fibonacci Index Solver
#  This section was the main part of Lab 0x01. The lab focuses on the 
#  Fibonacci sequence as a way to further understanding for basic programming.
#  This is accomplished with some basic commands and the appending of arrays. 
#  It begins with a test code which can be found at testlab1.my_string which is 
#  part of the \ref testlab1.py package. Please see Lab1.fib which is part of 
#  the \ref Lab1.py package to see the code.
#
#  @section sec_hw HW 0x00: Elevator Class Code
#  This... reference ElevatorAdapt.self which is part of \ref ElevatorAdapt.py
#
#  @author Tyler Guffey
#
#  @copyright TG-Coding
#
#  @date September 30, 2020
#