## @file testlab1.py
# This code mimics the fibonacci sequence.
#
# As the first lab, the fibonacci sequence is used here to provide 
# better practice with coding. The function created here provides the
# fibonacci number at a given index input.
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date September 30, 2020

import pyb

#@temp.py

## my_string is a simple code.
#
#  my_string asks for an input, aka 'string', and returns the text 'bar' if 
#  'foo' is entered as the input. This is more or less a lesson for dummies.
my_string = input('Please enter a string:')
if my_string == 'foo':
    print('bar')
else:
    pass