## @file Lab1.py
# This code mimics the fibonacci sequence.
#
# As the first lab, the fibonacci sequence is used here to provide 
# better practice with coding. The function created here provides the
# fibonacci number at a given index input.
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date September 30, 2020

import pyb

#@Lab1.py

## fib is a function that mimics the fibonacci sequence.
#
#  fib is a function that takes the given index number 'idx' and runs 
#  a for loop to collect an array of fibonacci numbers until it reaches the 
#  specified index position.
def fib (idx):
    '''
    @brief This is the main function of the file.
    @details This function embodies the fibonacci sequence through computing, 
             then indexing the values.
    
    '''
    ## ans_list is an index of fibonacci numbers.
    #
    #  ans_list is an array that is constantly updated by using its previous 
    #  terms to create each new index entry.
    ans_list = [0]
    if idx == 0:
        pass
    elif idx == 1:
        ansadd = 1
        ans_list.append(ansadd)
    elif idx>=2:
        ansadd = 1
        ans_list.append(ansadd)
        for x in range(1,idx):
            ansadd3= ans_list[x]+ans_list[x-1]
            ans_list.append(ansadd3)
    else:
        ans_list='neg'
    
    print('Calculating Fibonacci number at '
          'index n = {:}.'.format(idx))
    if ans_list=='neg':
        print('ERROR: Fibonacci number cannot be found for a negtive integer.')
    else:
        ans = ans_list[idx]
        print('The fibonacci number is {:d}'.format (ans))

## This line is meant to provide a quick verification.
#
#  The test output outputs the fibonacci number for a number that is easy to 
#  check in order to verify the validity of the code written for this lab.       
if __name__ == '__main__':
    
    fib(6)
    
