## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website is an online representation of all the works accomplished in
#  ME 305, for both the class and the lab; a portfolio of sorts. Below is a 
#  short description of each assignment that's been added and documented on 
#  this site. You may want to use this as a Table of Contents, and click the 
#  blue highlighted links for the code that you're interested in.
#
#  @section sec_enc Lab 0x01: Fibonacci Index Solver
#  This section was the main part of Lab 0x01. The lab focuses on the 
#  Fibonacci sequence as a way to further understanding for basic programming.
#  This is accomplished with some basic commands and the appending of arrays. 
#  It begins with a test code which can be found at testlab1.my_string which is 
#  part of the \ref testlab1.py package. Please see Lab1.fib which is part of 
#  the \ref Lab1.py package to see the code. For the full code, please click
#  the following url:
#  https://bitbucket.org/tjguffey/me-305/src/master/Lab%201/
#
#  @section sec_hw HW 0x00: Elevator Class Code
#  This is a homework assignment that explored classes and utilized an in 
#  class example to further identify ways to use them. In order to view the 
#  file documentation, and the state diagram drawn before implementing code, 
#  please reference ElevatorAdapt.self which is part of \ref ElevatorAdapt.py.
#  For access to the source code, please visit the following url:
#  https://bitbucket.org/tjguffey/me-305/src/master/HW/
#
#  @section sec_enc2 Lab 0x02: Hardware Introduction
#  This section was the entirety of Lab 2. The lab's main focus was on the 
#  integration of a Board and its LED along with the idea of becoming familiar 
#  with the coding experience. While the code looks like it should work, as of
#  now, the Nucleo will not respond to the files loaded. If you'd like to see
#  the documentation available for the FSM's developed, the main.py file used,
#  or the original Nucleo interface file used, click on the following links
#  respectively: \ref blinkpls.py , \ref main.py , and \ref nucleotest.py.
#  The source code for all three scripts can be found at the following url:
#  https://bitbucket.org/tjguffey/me-305/src/master/Lab%202/
#
#  @author Tyler Guffey
#
#  @copyright TG-Coding
#
#  @date September 30, 2020
#