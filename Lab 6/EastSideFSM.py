## @file EastSideFSM.py
# This file mimics the coding needed to operate a motor and encoder via the 
# Nucleo. A visual that helps demonstrate the process in shown here.
#
# @image html NucleoFSM6.jpg
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date November 23, 2020

import utime
from pyb import UART
from MotorDriver1 import MotorDriver
from Encoder import EncoderDriver

class EastSide:
    '''
    @brief      A finite state machine to control a motor.
    @details    This class implements a finite state machine to control the
                operation of a motor by use of an encoder input and gain.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_WAIT    = 1    
    
    ## Constant defining State 2
    S2_APPLY   = 2    
    
    ## Constant defining State 3
    S3_READ    = 3    
    
    ## Constant defining State 4
    S4_SEND    = 4
    
    ## Constant defining State 5
    S5_END     = 5
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, mottimer, pin1, pin2, enctimer):
        '''
        @brief            Creates a EastSide object.
        @param nSLEEP_pin
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The motor used in this process
        self.motor = MotorDriver(self.nSLEEP_pin, self.IN1_pin, self.IN2_pin, self.mottimer)
        
        ## The encoder used in this process
        self.encoder = EncoderDriver(self.pin1, self.pin2, self.enctimer)
        
        ## The input from other sources
        self.myuart = UART(2)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        #self.interval = 0.02
        self.interval = int(0.2*1e6)
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The number of encoder ticks per degree
        self.conv = 1000*4*4/360
        
    def run(self,value):
        '''
        @brief     Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            ## This code is launched when the system is started.
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_ADAPT)
            
            ## This part waits for some sort of user input
            elif(self.state == self.S1_WAIT):
                if self.runs*self.interval/1e6>5:
                    self.transitionTo(self.S5_END)
                elif self.myuart() !=0:
                    self.savechar()
                    self.transitionTo(self.S2_APPLY)
                ## Make a timer to keep track of inactivity
                self.runs += 1
            
            ## If we recieved a value, change the motor duty and recieve initial
            elif(self.state == self.S2_APPLY):
                ## Reset self.runs so it keeps running
                self.runs = 0
                ## Set motor duty to new duty value
                self.motor.set_duty(self.input)
                self.encoder.get_delta()
                self.transitionTo(self.S3_READ)
            

            elif(self.state == self.S3_READ): # Should be sent back a new load
                ## Gain a delta from motor
                self.encoder.get_delta()
                self.output = self.encoder.delta
                ## Change Encoder Ticks to Correct Units
                self.omega = self.output/self.conv/self.interval*1e6 # Change to get back to the right units
                
            elif(self.state == self.S4_SEND):
                self.sendchar(self.omega)
                self.transitionTo(self.S1_WAIT)
                
            elif(self.state == self.S5_END):
                pass
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
     
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


    def sendchar(self,inv):
        '''
        @brief     Sends an input to the serial
        '''
        #inv = input('If you wish to continue, please press G; Enter any other key to cancel: ')
        #self.serial.write(str(inv).encode('ascii'))
        print(inv)


    def savechar(self):
        '''
        @brief     Saves two character detected in the serial into two matrices
        '''
        #input_list = []
        if self.myuart() !=0:
            #ser.readline().decode('ascii') from Piazza
            self.input = self.myuart.readchar()#serial.readline()
            #input_list = input_string.strip('\r\n').split('\r\n')#split(',') or strip('\r\n')?
            #self.xvals.append(float(input_list[0]))
            #self.yvals.append(float(input_list[1]))
            





