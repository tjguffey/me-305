'''
@file ClosedLoop.py
This file provides the coding needed to operate an motor and encoder.

This file runs a FSM that will be used to regulate the Kp value applied to our
motors, and the results of the system will be plotted at the end of the allotted
time.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 23, 2020
'''

import time
import serial
import plt

class ClosedLoop:
    '''
    @brief      A finite state machine to control a motor.
    @details    This class implements a finite state machine to control the
                operation of a motor by use of an encoder input and gain.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_ADAPT   = 1    
    
    ## Constant defining State 2
    S2_SEND    = 2    
    
    ## Constant defining State 3
    S3_WAIT    = 3    
    
    ## Constant defining State 4
    S4_PREP = 4
    
    ## Constant defining State 5
    S5_PLOT    = 5
    
    def __init__(self, load, ref, kp, length, port):
        '''
        @brief            Creates a ClosedLoop object.
        @param load       An object from class ClosedLoop representing the Initial Load (L)
        @param ref        An object from class ClosedLoop representing the reference omega
        @param kp         An object from class ClosedLoop representing the gain factor (kp')
        @param length     An object from class ClosedLoop representing the length of time to run 
        @param port       An object from class Serial representing the serial port used
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The inital load we want to set the motor at; this will be updated
        self.load = self.load
        
        ## This is the reference omega
        self.ref = self.ref
        
        ## This is the kp or gain
        self.kp = self.kp
        
        ## The overall length of time the motor is regulated
        self.length = self.length
        
        ## The serial port set up for this lab
        ser = self.port  #Should be '/dev/tty.usbmodem14103'
        self.serial = serial.Serial(port = ser, baudrate = 115273, timeout = 1)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = 0.02
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self,value):
        '''
        @brief     Runs one iteration of the task
        @details   ClosedLoop.run() will first check the duty meant to be applied,
                   and it will assume that a value between 0 and 1 is a mistake
                   and corrects it by a factor of 100. This also will keep a
                   duty cycle of more than 100 to be limited to 100. Next, this 
                   value is sent to the Nucleo, and the duty cycle is applied 
                   to the motor. After this, the Nucleo waits for a response,
                   and once one is recieved, the operation analyzes the speed
                   of the motor and adjusts the duty cycle as it sees fit.
                   After this new duty is determined, the cycle is repeated.
                   After a certain interval of time, the program stops sending
                   out corrected values and plots the changes of the system.
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            ## This code is launched when the system is started.
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_ADAPT)
            
            ## This code is launched when the system is moving down.
            elif(self.state == self.S1_ADAPT):
                if self.load<1 and self.load > 0:
                    self.load = self.load*100
                elif self.load>-1 and self.load < 0:
                    self.load = self.load*100
                elif self.load>100:
                    self.load = 100
                elif self.load<-100:
                    self.load = -100
                else:
                    pass
                self.transitionTo(self.S2_SEND)
            

            elif(self.state == self.S2_SEND):
                # Run State 2 Code
                self.sendchar(self.load)
                self.transitionTO(self.S3_WAIT)
            

            elif(self.state == self.S3_WAIT): # Should be sent back a new load
                if self.runs*self.interval>self.length:
                    self.transitionTo(self.S5_PLOT)
                else:
                    self.savechar()
                    self.load = self.kp*(self.ref-self.yvals[-1])
                    self.transitionTo(self.S1_ADAPT)


            elif(self.state == self.S4_PREP):
                self.xvals = self.xvals
                self.yvals = self.yvals
                self.yvals2 = self.yvals #(Modified to only plot self.ref)
                self.transitionTo(self.S5_PLOT)
                
            elif(self.state == self.S5_DO_NOTHING):
                self.plot4u(self.xvals, self.yvals)
                #plot other line on same graph
                self.transitionTo(self.S6_END)
            
            elif (self.state == self.S6_END):
                None
                # Some clever exit strategy
            
            else:
                # Invalid state code (error handling)
                pass
                
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
     
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState


    def sendchar(self,inv):
        '''
        @brief     Sends an input to the serial
        '''
        #inv = input('If you wish to continue, please press G; Enter any other key to cancel: ')
        self.serial.write(str(inv).encode('ascii')) 


    def savechar(self):
        '''
        @brief     Saves two character detected in the serial into two matrices
        '''
        input_list = []
        if self.serial.readline() !=0:
            #ser.readline().decode('ascii') from Piazza
            input_string = self.serial.readline()
            input_list = input_string.strip('\r\n').split('\r\n')#split(',') or strip('\r\n')?
            self.xvals.append(float(input_list[0]))
            self.yvals.append(float(input_list[1]))
            

    def plot4u(self, one, two):
        '''
        @brief     Plots a graph with the below labels
        '''
        plt.plot(one, two, 'r-')
        plt.xlabel('Time [Seconds]')
        plt.ylabel('Position [Encoder Ticks]')
        plt.show()   


'''
Below we define the inital duty load, reference omega, gain factor, length of
time for the cycle, and the port that the Nucleo is connected to.
'''

load = 35
ref = 30
kp = 0.1
length = 5
port = '/dev/tty.usbmodem14103'

'''
Below we define the ClosedLoop operation as 'task' and we run it.
'''
task = ClosedLoop(load, ref, kp, length, port)
task.run()









