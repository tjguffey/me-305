'''
@file MotorDriver1.py

This file serves as the Motor Driver Class and will set up our Model DRV8847
DC Motor. It will also be able to enable, disable, and set the duty cycle of
the motor.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 12, 2020
'''

import pyb
class MotorDriver:
    ''' This class implements a motor driver for the ME305 board. '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer): 
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on 
                            IN1_pin and IN2_pin.
        '''
        
        ## The nSLEEP pin set up and started 'off' or 'low'
        self.pinnSleep = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinnSleep.low()
        
        ## The timer used for sending PWM Pulses to the Motor
        self.tim = pyb.Timer(timer, freq=20000)
        
        ## The IN1 pin setup
        self.in1 = pyb.Pin(IN1_pin)
        
        ## The IN2 pin setup
        self.in2 = pyb.Pin(IN2_pin)
        
        ## The Channel Setup for IN1
        self.timch1 = self.tim.channel(3,pyb.Timer.PWM, pin=self.in1)
        
        ## The Channel Setup for IN2
        self.timch2 = self.tim.channel(4, pyb.Timer.PWM, pin=self.in2)
        
        ## Inital Setup for the Motor to Spin Forward at 35% Duty
        self.timch1.pulse_width_percent(35)
        self.timch2.pulse_width_percent(0)
        
        print ('Creating a motor driver') 

    ## Turns the motor on at the specified speed
    def enable (self):
        '''
        @brief     This will activate the motor at 35% Duty
        '''
        self.pinnSleep.high()
        print ('Motor Enabled') 
    
    ## Turns the motor off
    def disable (self):
        '''
        @brief     This will deactivate the motor, which will cause it to come to a stop
        '''
        self.pinnSleep.low()
        print ('Motor Disabled')
    
    def set_duty (self, duty):
        ''' 
        @brief       This will set the duty used by the motor
        @details     This method sets the duty cycle to be sent to the motor to the 
                     given level. Positive values cause effort in one direction, negative 
                     values in the opposite direction.
        @param duty  A signed integer holding the duty cycle of the PWM signal sent to the motor '''
            
        if duty == 0:
            self.disable()
        elif duty>0:
            dutyuse = int(duty)
            self.timch1.pulse_width_percent(dutyuse)
            self.timch2.pulse_width_percent(0)
            self.enable()
        else:
            dutyabs = abs(int(duty))
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(int(dutyabs))
            self.enable()
            

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. 
    # Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run. Create the pin objects
    # used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin.cpu.A15; 
    pin_IN1 = pyb.Pin.cpu.B0; 
    pin_IN2 = pyb.Pin.cpu.B1;
    
    # Create the timer object used for PWM generation
    tim = 3;
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    #moe.set_duty(10)