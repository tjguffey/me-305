## @file blinkpls.py
#  This code defines two FSM's with the intent of 'blinking' a light.
#
#  This file serves as an example implementation of a finite-state-machine using
#  Python. The first example will use a finite state machine to simulate the code 
#  necessary to blink an LED and will be used as help later. There is a second 
#  finite state machine introduced second, and this FSM will be used to attempt
#  to blink lights on a Nucleo board in a pattern as dictated in the below image.
#
#  @image html spike.png
#
#  @author Tyler J. Guffey
#
#  @copyright TG-Coding
#
#  @date October 13, 2020


import utime, pyb, time

class testblink:
    '''
    @brief      A finite state machine to 'blink' a theoretical LED.
    @details    This class implements a finite state machine to mimic the
                operation of an LED. This code will later be adapted to 
                (hopefully) operate an LED on a Nucleo board.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT= 0
    
    ## Constant defining State 1
    S1_ON  = 1
    
    ## Constant defining State 2
    S2_OFF = 2
    
    ## A definition of the variables and function
    #
    # The below code takes two inputs; interval between flashes and the length
    # of time this code should run.
    def __init__(self, interval, length):
        '''
        @brief      Creates a testblink object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval  
    
        ## The overall length of time for this code to run
        self.length = length
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        while (self.length>(self.runs-1)*self.interval):
            # Run State 0 Code
            if(self.state == self.S0_INIT):
                print('START')
                time.sleep(self.interval)
                if (self.runs==0):
                    self.transitionTo(self.S1_ON)
                    
                        
            elif(self.state == self.S1_ON):
                # Run State 1 Code
                print('ON')
                time.sleep(self.interval)
                if (self.runs % 2) == 0:
                    print('ERROR')
                else:
                    self.transitionTo(self.S2_OFF)
                           
            elif(self.state == self.S2_OFF):
                # Run State 2 Code
                print('OFF')
                time.sleep(self.interval)
                if (self.runs-round(self.runs/2) == 0.5*self.runs):
                    self.transitionTo(self.S1_ON)
                            
            else:
                # Undefined state for Error handling
                pass
                        
            self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
        
class realblink:
    '''
    @brief      A finite state machine to 'blink' an actual LED.
    @details    This class implements a finite state machine to manage the
                operation of an LED. This code is directly adapted from above 
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT= 0
    
    ## Constant defining State 1
    S1_MIN  = 1
    
    ## Constant defining State 2
    S2_MIN = 2
    
    ## Constant defining State 3
    S3_MIN = 3
    
    ## Constant defining State 4
    S4_MIN = 4
    
    ## Constant defining State 5
    S5_MIN = 5
    
    ## Constant defining State 6
    S6_MAX = 6
    
    ## Constant defining State 7
    S7_MAX = 7
    
    ## Constant defining State 8
    S8_MAX = 8
    
    ## Constant defining State 9
    S9_MAX = 9
    
    ## Constant defining State 10
    S10_MAX = 10
    
    ## Constant defining stopped condition
    S11_STOP = 11
    
    def __init__(self, inter, length):
        '''
        @brief      Creates a realkblink object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The interval of time, in seconds, between runs of the task 
        self.interval = int(inter*1e6)
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
    
        ## The overall length of time for this code to run
        self.length = length#50
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            
        # State 0 Code (0%)
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time and self.state != self.S11_STOP:
            if(self.state == self.S0_INIT):
                # Run State 0 Code (Start Message)
                print('Ready Player One')
                t2ch1.pulse_width_percent(0)
                self.transitionTo(self.S1_MIN)
                self.next_time = utime.ticks_add(self.next_time, self.interval) 
                
            ## State 1 Code (10%)            
            elif(self.state == self.S1_MIN):
                # Run State 1 Code
                t2ch1.pulse_width_percent(10)
                self.transitionTo(self.S2_MIN)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 2 Code (20%)
            elif(self.state == self.S2_MIN):
                # Run State 2 Code
                t2ch1.pulse_width_percent(20)
                self.transitionTo(self.S3_MIN)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 3 Code (30%)
            elif(self.state == self.S3_MIN):
                # Run State 2 Code
                t2ch1.pulse_width_percent(30)
                self.transitionTo(self.S4_MIN)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 4 Code (40%)
            elif(self.state == self.S4_MIN):
                # Run State 2 Code
                t2ch1.pulse_width_percent(40)
                self.transitionTo(self.S5_MIN)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 5 Code (50%)
            elif(self.state == self.S5_MIN):
                # Run State 2 Code
                t2ch1.pulse_width_percent(50)
                self.transitionTo(self.S6_MAX)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 6 Code (60%)
            elif(self.state == self.S6_MAX):
                # Run State 2 Code
                t2ch1.pulse_width_percent(60)
                self.transitionTo(self.S7_MAX)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 7 Code (70%)
            elif(self.state == self.S7_MAX):
                # Run State 2 Code
                t2ch1.pulse_width_percent(70)
                self.transitionTo(self.S8_MAX)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 8 Code (80%)
            elif(self.state == self.S8_MAX):
                # Run State 2 Code
                t2ch1.pulse_width_percent(80)
                self.transitionTo(self.S9_MAX)
                
            ## State 9 Code (90%)
            elif(self.state == self.S9_MAX):
                # Run State 2 Code
                t2ch1.pulse_width_percent(90)
                self.transitionTo(self.S10_MAX)
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            ## State 10 Code (100%)
            elif(self.state == self.S10_MAX):
                # Run State 2 Code
                t2ch1.pulse_width_percent(100)
                self.transitionTo(self.S1_MIN)  
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            self.runs += 1
            #self.next_time = utime.ticks_add(self.next_time, self.interval)
            
        elif self.runs*self.interval/1e6>self.length:
           self.transitionTo(self.S11_STOP)
           t2ch1.pulse_width_percent(0) 
           
        else:
            # Undefined state for Error handling
            pass    
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
