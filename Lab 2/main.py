'''
@file main.py

This file serves as middle-'man' between the user and the Nucleo Board in use.
While there is not much educational value of use here, it is included for your 
reference with hopes that it is beneficial to you anyways.

@author Tyler J. Guffey

@copyright TG-Coding

@date October 13, 2020

'''

## Importing FSM from our Source File
#
# Both of our FSM's for this lab were fairly long, and we wanted to provide an
# uncluttered space to run our code in. In order to keep this file looking
# clean, we import the classes below.
from blinkpls import testblink, realblink
#import time

## Task objects
#
# The first task is run for our 'blinking' text; we need to specify how long
# it should stay in each state and how long we want to simulate this for.Because 
# the variables are already well defined in the class we have nothing that
# needs to be defined in the parenthesis.
#task1 = testblink(0.25, 2) # Will also run constructor

## The second task is meant to run the actual blinking light. 
#
# Because the variables are already well defined in the class we have nothing that
# needs to be defined in the parenthesis of the command.
task2 = realblink(1,10)

## To run the first task, we call task1.run()
#
# First we run this task, and the display should be very simple. The display
# show 9 outputs (start + 4 cycles of on and off). However, in order to
# provide a slight break on the users eyes, we also will put a slight delay 
# before running task2. Task2 will actually display lights on the Nucleo board.
#task1.run ()
#time.sleep(3)
while True:
    task2.run ()

