'''
@file positionmecap.py

This is the main.py file that will be used by the Nucleo to execute Lab 4.

The code takes data from the encoder at 5 Hz, and proceeds to save these data
points as both time and position for later use.


@author Tyler J. Guffey

@copyright TG-Coding

@date November 12, 2020
'''




from pyb import UART
import pyb
import utime
myuart = UART(2)

class position:
    '''
    @brief     This is a class for tracking the position of the motor
    @details   This class implements a finite state machine that can track the
               position of the motor and return this positioning data to the
               user.
    '''
    ## Initialization State, We Start Here
    S0_INIT = 0
    
    ## State One, Data Collection
    S1_COLLECT = 1
    
    ## State Two, Terminate Code
    S2_PUSH = 2
    
    ## State Three, Terminate Code
    S3_END = 3
    
    def __init__(self):
        '''
        @brief            Creates a position object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in milliseconds between runs of the task (Period)
        #self.interval = 1/5
        self.interval = int(0.2*1e6)
        
        ## The amount of time in milliseconds between data set transmissions
        self.interval2 = int(0.1*1e6)
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The timer used to read the Encoder state and associated counter
        self.tim1 = pyb.Timer(3)    #3
        self.tim1.init(prescaler=0, period=65535)
 	
        ## The channel used for object encoder1
        self.encoder1 = self.tim1.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
 	
        ## The channel used for object encoder2
        self.encoder2 = self.tim1.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB) #Encoder 1, Diff Channel

        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Counter that describes the number of times the push task has run
        self.runs2 = 0
        
        ## The Initial Position of Start-Up
        self.currentp = self.tim1.counter()
        
        ## The Initial Time of Start-Up
        # self.currentt = time.time()
        self.currentt = utime.ticks_us()
        
        ## The time since starting the command
        #self.time = time.time()-self.start_time
        self.time = utime.ticks_us()-self.start_time
        
        ## The list that will be continuously sent back to the computer
        self.list = []
        
        #print('Encoder Initialized')
        
    def run(self):
        '''
        @brief    This runs the Finite State Machine to take Data
        @details  position.run() activates the FSM based on input received from
                  Spyder and returns all the data taken after 10 seconds.
        '''
        
        ## Run the Initial State Code
        if self.state== self.S0_INIT:
            
            ## First we check the uart for input
            if myuart.any():
                ## Read the UART character into an ASCI character
                readc = myuart.readchar()
                
                ## If that character is a G, the code moves to data collection
                if readc == 71:
                    #self.start = time.time()
                    self.start = utime.ticks_us()
                    self.currentp = self.tim1.counter()
                    #self.list = [self.start, self.currentp]
                    self.transitionTo(self.S1_COLLECT)
                    
                ## If that character is any other character, the Nucleo stops waiting for user input    
                elif readc != 71:
                    #print('Thanks, Come Again Soon')
                    self.transitionTo(self.S3_END)
                    
        ## Run the Data Collection State
        elif self.state==self.S1_COLLECT:
            ## Check the current time
            self.currentt = utime.ticks_us()
            
            ## If the code has run for slightly more than 10 seconds, the data collection will stop
            if self.runs*self.interval/1e6>10.1:
                self.transitionTo(self.S2_PUSH)
                self.next_time = utime.ticks_add(self.next_time, self.interval2)
                #print('Data Collection Complete')
                
            ## If the current time is more than the predetermined next time to run, continue
            elif self.currentt > self.next_time:
                
                ## Check for new inputs
                if myuart.any():
                    ## If there is a character, make it usable in ASCI
                    readc = myuart.readchar()
                    
                    ## If a capital S is detected, the data collection is ended
                    if readc == 83: #CAPITAL S
                        self.transitionTo(self.S2_PUSH)
                        #print('Data Collection Terminated')
                        
                    ## If there is any other letter input, the code continues taking data
                    elif readc != 83:
                        pass
                        #print('Please Type S or Wait for Data Collection To Finish')
                
                ## If there is no character input, the encoder position is read and saved
                else:
                    self.update()
                    self.saveval(self.time/1e6, self.currentp)
                    self.runs += 1
                    #self.next_time = self.next_time+self.interval
                    
                    ## The first interval adjusts the next time to run to be based off of when data collection is started
                    if self.runs==1:
                        self.next_time = utime.ticks_add(self.currentt, self.interval)
                    
                    ## All other intervals are adjusted to be based off of the last determined interval
                    else:
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
        ## Run the Data Push segment of code            
        elif self.state==self.S2_PUSH:
            ## Take the current time
            self.time2 = utime.ticks_us()
            
            ## If the time is past a predetermined time, continue
            if self.time2>self.next_time:
            #if self.runs2 <=(len(self.list)-1)/2-1:
                #if self.time2>self.next_time:
                    
                ## The time and position are indexed to send one at a time in the same category
                if self.runs2 <=(len(self.list))/2-1: 
                    self.sendval(2*self.runs2, 2*self.runs2+1)
                    
                ## Once the FSM has run through the entire index of data taken, the data push is ended
                else:
                    self.transitionTo(self.S3_END)
                self.next_time = utime.ticks_add(self.next_time, self.interval2)
                self.runs2 += 1
        
        ## Run the Finished State        
        elif self.state==self.S3_END:
            ## The code is finished and so has nothing to do. It will stay in this state until reset or shutdown
            pass
        
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def update(self):
        '''
        @brief    Updates the current position read by the encoder
        '''
        self.currentp = self.tim1.counter()
        #self.time = time.time()-self.start_time
        self.time = utime.ticks_us()-self.start
    
    def saveval(self,one,two):
        '''
        @brief   Saves the most recent values to the position array
        '''
        self.list.append(one)
        self.list.append(two)
    
    def sendval(self,one,two):
        '''
        @brief   Sends the most recent values back to the system
        '''
        xvals = self.list[one]
        yvals = self.list[two]
        myuart.write('{:}, {:}\r\n'.format(xvals, yvals))
        #myuart.write('{:}, {:}\r\n'.format(self.list[one], self.list[two]))
        #myuart.write('{:}, {:}\r\n'.format(one, two))