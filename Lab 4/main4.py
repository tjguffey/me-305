'''
@file main4.py

This is the main.py file that will be used by the Nucleo to execute Lab 4.
It simply operates the position class as defined in said class.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 12, 2020
'''




from pyb import UART
from positionmecap import position

## Define the user terminal as 'myuart'
myuart = UART(2)

## Define a variable to be true
x = True

## While this variable is true, run the following commands
while x():
    
    ## Check to see if there are any inputs from the serial port
    if myuart.any() != 0:
        
        ## This defines the position class as task1
        task1 = position()
        
        #val = myuart.readchar()
        #myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        
        ## This performs the run() operater of class position
        task1.run() #position.run(task1)
        
        ## The Nucleo exits the cycle and is ready for other operations
        x = False
        
    