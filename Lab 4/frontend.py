'''
@file frontend.py

This is the main.py file that will be used by the Nucleo to execute Lab 4.
'''



import serial
import time
import matplotlib.pyplot as plt

class frontend:
    '''
    @brief
    '''
    ## Initialization State, We Start Here
    S0_INIT = 0
    
    ## State One, Data Collection
    #S1_START = 1
    
    ## State Two, Wait for NUCLEO to finish
    S2_WAIT = 2
    
    ## State Three, Obtain Code
    S3_COLLECT = 3
    
    ## State Four, Plot the Data
    S4_PLOT = 4
    
    ## Terminating the Serial Communication
    S5_END = 5
    
    def __init__(self):
        '''
        @brief Creates a frontend object
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The serial port we're using
        self.ser = serial.Serial(port = '/dev/tty.usbmodem14103', baudrate = 115273, timeout = 1)
        
        ## The serial port responses
        self.read = self.ser.readline()
        
        ##  The amount of time in milliseconds between runs of the task (Period)
        self.interval = 1/5
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval

        ## The list that will be continuously sent back to the computer
        self.list = []
        
    def run(self):
        '''
        @brief
        '''
        if self.state== self.S0_INIT:
            self.sendchar()
            self.transitionTo(self.S2_WAIT)
        
        elif self.state==self.S2_WAIT:
            self.read = self.ser.readline()
            if self.read():
                    self.transitionTo(self.S3_COLLECT)
                    self.next_time  = time.time()
                    self.start_time = time.time()
                    
        elif self.state==self.S3_COLLECT:
            self.now = time.time()
            if self.now-self.start_time>10.5:
                self.transitionTo(self.S5_END)
            elif self.read():#now>self.next_time:
                self.saveval()
                self.next_time += self.interval

                    
        elif self.state==self.S4_PLOT:
            self.plot4u(self.x, self.y)
            self.transitionTo(self.S5_END)
            self.ser.close()
            
        elif self.state==self.S5_END:
            pass
        
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    def sendchar(self):
        '''
        @brief   Sends the most recent values back to the system
        '''
        inv = input('If you wish to continue, please press G; Enter any other key to cancel: ')
        self.ser.write(str(inv).encode('ascii')) 
        #myval = ser.readline().decode('ascii') 
        #return myval
        
    def saveval(self):
        '''
        @brief   Saves the most recent values to the position array
        '''
        input_list = []
        input_list = self.ser.readline().decode('ascii').strip('\r\n').split(',')
        self.x.append(int(input_list[0]))
        self.y.append(int(input_list[1]))
        
    def plot4u(self, one, two):
        plt.plot(one, two, 'r-')
        plt.xlabel('Time [Seconds]')
        plt.ylabel('Position [Encoder Ticks]')