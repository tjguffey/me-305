'''
@file Lab4.py

This file serves as the main function file that will take inputs from Spyder
and present the encoder movement as a function of time for the user.

This code does this by sending the input to the Nucleo board and the Nucleo
proceeds to take in data for 10 seconds.

@author Tyler J. Guffey

@copyright TG-Coding

@date November 7, 2020
'''

import serial
import time
import matplotlib.pyplot as plt

## Define the serial port that the Nucleo is attached to
ser = serial.Serial(port = '/dev/tty.usbmodem14103', baudrate = 115273, timeout = 1)

## The initially empty matrix for our time data
xvals = []

## The initially empty matrix for our encoder position data
yvals = []

## Check the serial port at this interval; should match input frequency
interval = 0.1

## Rewind the Preset by 9 seconds; should make it so rounds*interval = 1 when data appears, then it'll be set back to 0
rounds = -9/interval

## This will have the loop stuck for a while until y is False
y = True

## This will send an input to the Nucleo; it also shows a prompt for the user
def sendchar():
    inv = input('If you wish to continue, please press G; Enter any other key to cancel: ')
    ser.write(str(inv).encode('ascii')) 
    #n = 0
    #for n in range(51):
        #data_string = ser.readline().decode('ascii')

## This saves a set of data (in pairs)
def savechar():
    input_list = []
    if ser.readline() !=0:
        #ser.readline().decode('ascii') from Piazza
        input_string = ser.readline()
        input_list = input_string.strip('\r\n').split('\r\n')#split(',') or strip('\r\n')?
        xvals.append(float(input_list[0]))
        yvals.append(float(input_list[1]))
            
    
## This will plot the graph with a nice format
def plot4u(one, two):
        plt.plot(one, two, 'r-')
        plt.xlabel('Time [Seconds]')
        plt.ylabel('Position [Encoder Ticks]')
        plt.show()
        
## Print the input request, send input to serial port              
sendchar()

## The start of our next loop
start = time.time()

## The next time to check
next_time = start+interval

while y==True:
    current = time.time()
    if current>=next_time:
        ## If somethings in the serial read, count rounds as 0
        if ser.readline() !=0:
            rounds = 0
            savechar()
        ## If theres nothing in the serial read, start adding
        else:
            rounds +=1
        next_time += interval
    ## If its beem three seconds since rounds evaluated to zero, cancel loop
    elif rounds*interval>=3:
        y = False
    
## This will use our plotting command to hopefully plot and format
plot4u(xvals, yvals)

## This will close the serial port   
ser.close()

## Sending Values
#https://piazza.com/class/keg2fs1rmr935q?cid=44