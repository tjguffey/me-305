## @file ElevatorAdapt.py
# This file mimics the coding needed to operate an elevator.
#
# This homework assignment uses a class system to write code that will operate 
# an elevator with minimal user input. The code will keep in mind what floor
# the elevator is on, go to the floor desired by a user, and will not move
# until necessary.
#
# There are two buttons, one that picks the top floor and one that picks the 
# bottom floor. These buttons are user inputs and will cause the elevator to 
# move.
#
# There are two indicators that will keep track of the elevators position for 
# both the necessary procedure of the code and the convenience of the user.
#
# The following illustration is meant to show the ideation process used before
# coding in the desired inputs and outputs. It was hand drawn, so please allow
# the minor inconsistencies.
#
# @image html state_diag.jpg
#
# @author Tyler J. Guffey
#
# @copyright TG-Coding
#
# @date October 7, 2020

from random import choice
import time

class ElevatorAdapt:
    '''
    @brief      A finite state machine to control an elevator with two floors.
    @details    This class implements a finite state machine to control the
                operation of an elevator with user input and sensors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_ON_1      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_ON_2     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def __init__(self, interval, button_1, button_2, first, second, Motor):
        '''
        @brief            Creates a ElevatorAdapt object.
        @param button_1   An object from class Button representing the floor one button.
        @param button_2   An object from class Button representing the floor two button.
        @param first      An object from class Button representing the First Floor Indicator.
        @param second     An object from class Button representing the Second Floor Indicator.
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for going down
        self.button_1 = button_1
        
        ## The button object used for going up
        self.button_2 = button_2
        
        ## The button object indicating floor 1
        self.first = False
        
        ## The button object indicating floor 2
        self.second = True
        
        ## The motor object moving the elevator system
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## The time at the start of each loop
        self.curr_time = time.time()
        ## Checking the time to loop at a certain interval
        if self.curr_time > self.next_time:
            
            ## This code is launched when the system is started.
            #
            # The first few lines prepare the elevator for use and makes sure
            # that if the elevator isn't on the first floor, that it will get 
            # there for use.
            if(self.state == self.S0_INIT):
                # Run State 0 code
                ## Checks if the elevator starts on the first floor
                #
                # If the elevator is already on the first floor, it will be
                # moved onto State 3 and the motor will be turned off.
                if(self.first.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_ON_1)
                    self.Motor.STOP
                ## Checks if the elevator starts on the second floor
                #
                # If the elevator is started on the second floor, it will be 
                # moved into State 1 and the motor will move the elevator 
                # "down"
                else:
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.DOWN()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            ## This code is launched when the system is moving down.
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                ## This is from a class time example that I didn't change
                if(self.runs*self.interval > 15):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                ## This code checks if the elevator has reached the first floor
                #
                # The following code only applies when the elevator has 
                # reached the first floor. It will stop the motor and move the
                # code to State 3.
                if(self.first.getButtonState()):
                    self.Motor.STOP()
                    self.transitionTo(self.S3_STOPPED_ON_1)
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            ## This code is launched when the system is moving up.
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                ## This code checks if the second floor has been reached.
                #
                # The following code only applies when the elevator has 
                # reached the second floor. It will stop the motor and move the
                # code to State 2.
                if(self.second.getButtonState()):
                    self.Motor.STOP()
                    self.transitionTo(self.S4_STOPPED_ON_2)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            ## This code is launched when the elevator stops on the first floor
            elif(self.state == self.S3_STOPPED_ON_1):
                # Transition to state 2 if button_2 is True
                ## This code only applies after button_2 has been activated.
                #
                # The following code looks to see if the button to go up has 
                # been pressed; if it has, the motor is activated, the code is
                # moved to State 2. and the button is turned back off.
                if(self.button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.UP()
                    self.button_2 = False
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            ## This code is launched when the elevator stops on the 2nd floor.
            elif(self.state == self.S4_STOPPED_ON_2):
                # Run State 4 Code
                ## This code only applies after button_1 has been activated.
                #
                # The following code looks to see if the button to go down has 
                # been pressed; if it has, the motor is activated, the code is
                # moved to State 4. and the button is turned back off.
                if(self.button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.DOWN()
                    self.button_1 = False
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            ## This code is more or less a fail safe if anything doesnt work.    
            elif(self.state == self.S5_DO_NOTHING):
                # Run State 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            ## Increase the amount of runs experienced to keep track of times ran
            self.runs += 1
            
            ## Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])
 
class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                move up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def UP(self):
        '''
        @brief Moves the motor upward
        '''
        print('Motor moving Up')
    
    def DOWN(self):
        '''
        @brief Moves the motor down
        '''
        print('Motor moving Down')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stopped')



























